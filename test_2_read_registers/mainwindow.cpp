#include "mainwindow.h"
#include "ui_mainwindow.h"


#include <QtGui>
#if QT_VERSION >= QT_VERSION_CHECK(5, 0, 0)
#include <QtWidgets>
#endif
#include <QtSerialPort/QSerialPortInfo>

#include "quick_modbus.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
  , runing(false)
  , _errorCounter(0)
{
    ui->setupUi(this);

    clearTable();

    connect(ui->updatePorts, &QToolButton::clicked, this, [=](){
        ui->comboPort->clear();
        foreach (QSerialPortInfo inf, QSerialPortInfo::availablePorts()) {
            ui->comboPort->addItem(inf.portName(), inf.productIdentifier());
        }
    });

    ui->updatePorts->click();

    connect(ui->comboPort, SIGNAL(currentIndexChanged(QString)), this, SLOT(changePort(QString)));


    _transport = new Transport(this);
    _transport->setPortName(ui->comboPort->currentText());

    _transport->setSpeed(57600);
    _transport->setDataBits(8);
    _transport->setParity(QSerialPort::NoParity);
    _transport->setStopBits(1.0);

    connect(ui->buttonConnect, SIGNAL(toggled(bool)), this, SLOT(connectDevice(bool)));

    connect(ui->buttonRun, SIGNAL(clicked(bool)), this, SLOT(run()));
    connect(ui->buttonStop, SIGNAL(clicked(bool)), this, SLOT(stop()));

    _loopTimer = new QTimer(this);
    _loopTimer->setSingleShot(true);
    _loopTimer->setInterval(ui->spinInterval->value());
    connect(_loopTimer, SIGNAL(timeout()), this, SLOT(startSend()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::changePort(QString aname)
{
    _transport->setPortName(aname);
}

void MainWindow::connectDevice(bool aconnect)
{
    if (aconnect){
        MBDevice *dev = new MBDevice("test", _transport);
        dev->setAddress(ui->spinDeviceAddress->value());

        connect(dev, SIGNAL(responseReceived(const MBResponse&)), this, SLOT(setResponse(const MBResponse&)));
        connect(dev, SIGNAL(errorOccurred(const MBError&)), this, SLOT(setError(const MBError&)));

        ui->comboPort->setDisabled(true);
        ui->updatePorts->setDisabled(true);
        ui->spinDeviceAddress->setDisabled(true);

        _transport->open();
    }else {
        MBDevice *dev = _transport->takeDevice("test");
        disconnect(dev, SIGNAL(responseReceived(const MBResponse&)), this, SLOT(setResponse(const MBResponse&)));
        disconnect(dev, SIGNAL(errorOccurred(const MBError&)), this, SLOT(setError(const MBError&)));

        ui->comboPort->setDisabled(false);
        ui->updatePorts->setDisabled(false);
        ui->spinDeviceAddress->setDisabled(false);

        _transport->close();
    }
}

void MainWindow::run()
{
    if (_transport->devices().isEmpty()){
        qDebug() << "No device";
        return;
    }

    clearTable();
    ui->error->setText("");
    _errorCounter = 0;
    _loopTimer->setInterval(ui->spinInterval->value());
    runing = true;
    if (ui->checkLoop->isChecked())
        ui->buttonRun->setDisabled(true);
    startSend();
}

void MainWindow::startSend()
{
    MBRequest req(MBTable::HoldingRegister, MBRequest::ModeRead);

    req.setStartAddress(ui->spinDataAddress->value());
    req.setDataCount(ui->spinDataCount->value());

    MBDevice *dev = _transport->device("test");
    if (!dev->sendRequest(req)){
        ui->error->setText(dev->error().errorString());
        stop();
    }

    _lastUpdate = QTime::currentTime();
}

void MainWindow::stop()
{
    ui->buttonRun->setEnabled(true);
//    _transport->close();
    _loopTimer->stop();
    runing = false;
}


void MainWindow::setResponse(const MBResponse &aresp)
{
    clearTable();
    qDebug() << "MainWindow::setResponse(const MBResponse &aresp):\n"  << aresp.data();
    ui->tableResult->setRowCount(aresp.data().size());
    int row = 0;

    QTime ct = QTime::currentTime();
    int interval = _lastUpdate.msecsTo(ct);
    _lastUpdate = ct;

    QList<int> keys = aresp.data().keys();
    qSort(keys);
    foreach (int addr, keys) {
        QTableWidgetItem *itemAddress = new QTableWidgetItem(tr("%1").arg(addr));
        ui->tableResult->setItem(row, 0, itemAddress);
        QTableWidgetItem *itemValue = new QTableWidgetItem(tr("%1").arg(aresp.data().value(addr)));
        ui->tableResult->setItem(row, 1, itemValue);
        QTableWidgetItem *itemInterval = new QTableWidgetItem(tr("%1").arg(interval));
        ui->tableResult->setItem(row, 2, itemInterval);
        row++;
    }

    if (runing){
        if  (ui->checkLoop->isChecked())
            _loopTimer->start();
        else
            runing = false;
    }
}

void MainWindow::setError(const MBError &aerr)
{
    qDebug() << "MainWindow::setError(const MBError &aerr)";
    _errorCounter++;
    ui->error->setText(aerr.errorString() + QString(" (%1)").arg(_errorCounter));

    if  (ui->checkLoop->isChecked())
        _loopTimer->start();
    else
        stop();
}

void MainWindow::clearTable()
{
    ui->tableResult->clear();
    ui->tableResult->setColumnCount(3);
    ui->tableResult->verticalHeader()->setVisible(false);
    QStringList headers;
    headers << "Addr" << "Data" << "Interval (ms)";
    ui->tableResult->setHorizontalHeaderLabels(headers);
}
