#include "mainwindow.h"
#include <QApplication>
#include <QTextCodec>
#include <QTextStream>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
        QTextCodec::setCodecForTr(QTextCodec::codecForName("utf-8"));
#endif

    QStringList arguments = qApp->arguments();
    if ((arguments.size() > 1) && (arguments.at(1).startsWith("-v"))){
        QTextStream out(stdout);
        out << "Git SHA1:" << GIT_SHA_VERSION << endl;
    }

    MainWindow w;
    w.show();

    return a.exec();
}
