#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTime>

#include "quick_modbus.h"

namespace Ui {
class MainWindow;
}

class Transport;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void changePort(QString aname);
    void connectDevice(bool aconnect);
    void startSend();
    void run();
    void stop();


private slots:
    void setResponse(const MBResponse &aresp);
    void setError(const MBError &aerr);

private:
    void clearTable();

private:
    Ui::MainWindow *ui;
    Transport *_transport;
    QTimer *_loopTimer;
    bool runing;
    QTime _lastUpdate;
    quint16 _errorCounter;
};

#endif // MAINWINDOW_H
