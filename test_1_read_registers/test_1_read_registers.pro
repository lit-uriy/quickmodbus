#-------------------------------------------------
#
# Project created by QtCreator 2019-05-28T09:27:11
#
#-------------------------------------------------

TARGET = QuickModbus2
TEMPLATE = app

HARDCODED += git_version

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        $$PWD/main.cpp \
        $$PWD/mainwindow.cpp

HEADERS += \
        $$PWD/mainwindow.h

FORMS += \
        $$PWD/mainwindow.ui


#========= Dependency ==============
#	--- Qt ---
QT      += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

#	--- serialport ---
greaterThan(QT_MAJOR_VERSION, 4){
    QT *= serialport
} else {
    CONFIG *= serialport
}

#	--- QuickModbus ---
DEPENDPATH  *= $$PWD/../qmb
INCLUDEPATH *= $$PWD/../qmb
SOURCES += \
        $$PWD/../qmb/quick_modbus.cpp

HEADERS += \
        $$PWD/../qmb/quick_modbus.h


#	--- Modbus CRC ---
MODBUS_CRC = D:\001-Work\03-Ru\ru_common\daq\3rdparty
SOURCES += $${MODBUS_CRC}\crc.cpp \
           $${MODBUS_CRC}\modebus\modbus_crc.c
HEADERS += $${MODBUS_CRC}\crc.h \
           $${MODBUS_CRC}\modebus\modbus_crc.h
DEPENDPATH  *= $${MODBUS_CRC}
INCLUDEPATH *= $${MODBUS_CRC}


#====== Convenient config =========
DEBUGCONFIG = false
include($$(SOFT_LIB)/myPri/stdconfig.pri)

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
