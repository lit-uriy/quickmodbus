#ifndef QUICK_MODBUS_H
#define QUICK_MODBUS_H

#include <QObject>
#include <QtSerialPort/QSerialPort>
#include <QHash>

class QStateMachine;
class QTimer;

class MBDevice;

class Transport : public QObject
{
    Q_OBJECT
public:
    explicit Transport(QObject *parent = nullptr);
    virtual ~Transport(){}

    void setPortName(QString aport);
    QString portName();


    // -- параметры COM-порта
    bool setSpeed(int aspeed);
    bool setDataBits(int adataBits);
    bool setStopBits(float astopBits);
    bool setParity(QSerialPort::Parity aparity);

    QSerialPort* port(){return _port;}

    bool open();
    void close();

    int interFrameDelayMs(){ return _interFrameDelayMs;}
    int responseTimeoutMs(){return _responseTimeoutMs;}

    QString errorString();

    void addDevice(MBDevice *adev, QString name);
    MBDevice* device(QString name);
    QHash<QString, MBDevice*> devices();
    MBDevice* takeDevice(QString name);

signals:

public slots:

private:
    void calcTimeouts();

private:
    QSerialPort *_port;
    int _speed;
    int _dataBits;
    float _stopBits;
    QSerialPort::Parity _parity;
    QHash<QString, MBDevice*> _devices; // Устройство - имя
    int _interFrameDelayMs;
    int _responseTimeoutMs;

    friend class MBDevice;
};

// -
enum MBTable {
    DiscretesInput = 1, // Single bit, Read-Only
    Coils,              // Single bit, Read-Write
    InputRegisters,     // 16-bit word, Read-Only
    HoldingRegister,    // 16-bit word, Read-Write
};

// -- Запрос
class MBRequest
{
public:
    enum MBRequestMode {
        ModeRead = 1,
        ModeWriteMultiple,
        ModeUnknown
    };

    MBRequest();
    MBRequest(MBTable atable, MBRequest::MBRequestMode amode);

    void setDataTable(MBTable atable);
    void setRequestMode(MBRequestMode amode);
    void setStartAddress(int adr);
    void setDataCount(int count);

    MBTable dataTable();
    MBRequest::MBRequestMode requestMode();
    int startAddress();
    int dataCount();

private:
    void init();
    void makePdu();

private:
    friend class MBDevice;
    friend class MBComparator;

    MBTable _table;
    MBRequestMode _mode;
    int _start;
    int _quantity;
    int _func;
    QByteArray _pdu;
    int _funcs[2][4];
};

// -- Ответ
class MBResponse
{
public:
    MBResponse() {}

    MBTable dataTable();
    int startAddress();
    int dataCount();

    QHash<int, int> data() const {return _data;}


private:
    friend class MBDevice;
    friend class MBComparator;

    QHash<int, int> _data; // <dataAdr, dataValue>
};

// -- Ошибка
enum ErrorCode {
    ErrorNone = 0,
    ErrorAnotherSlave,
    ErrorAnotherFunc,
    ErrorIncomplete,
    ErrorCrcMissmatch
};

class MBError
{
public:
    MBError() {}
    QString errorString() const {return _errorString;}


private:
    QString _errorString;

    friend class MBDevice;
    friend class MBComparator;
};


class MBComparator
{
public:
    MBComparator(MBRequest &areq, int addr);

    QByteArray requestAdu();
    void appendRxData(const QByteArray &ar);
    bool isOurAddress();
    bool isOurFunction();
    bool isAduFullLenth();
    bool isExceptionResponse();
    int exceptionCode();
    MBResponse response();
    ErrorCode state() {return ErrorCode(_state);}
    MBError error();
private:
    QByteArray crc(const QByteArray& array);
    void makeResponse();


private:
    MBRequest _req;
    MBResponse _resp;
    MBError _error;
    QByteArray _requestAdu;
    QByteArray _responseAdu;
    int _address;
    int _func;
    int _pduSizeGood;
    int _pduSizeExept;
    bool _fullLenth;
    int _state;
};

// -- Устройство
class MBDevice : public QObject
{
    Q_OBJECT
public:
    explicit MBDevice(QString aname);
    explicit MBDevice(QString aname, Transport *parentTransport = nullptr);
    virtual ~MBDevice() {}

    void setAddress(int adr);


    bool sendRequest(MBRequest areq);

    MBError error();

signals:
    void responseReceived(const MBResponse &aresp);
    void errorOccurred(const MBError &aerr);
    void exceptionReceived(int acode);

private:
    void init();

private slots:
    void processState();
    void rxTimeoutOccured();
    void rxInterframeTimeoutOccured();
    void portError(QSerialPort::SerialPortError aerr);

private:
    Transport *_transport;
    QSerialPort *_port;
    int _address;
    QString _name;

    MBRequest _request;
    QByteArray _requestAdu;

    MBComparator *_comparator;

    MBResponse _response;
    QByteArray _responseAdu;
    QTimer *_timerTimeout;
    QTimer *_timerInterFrame;

    MBError _error;

    enum State {
        StateIdle = 0,
        StateSend,
        StateSended,
        StateBroadcastDelay,
        StateWaitResponse,
        StateRxInProgress,
        StateRxFinished,
        StateResponseTimeout,
        StateInterFrameTimeout
    };

    int _state;

    friend class Transport;
};



#endif // QUICK_MODBUS_H
