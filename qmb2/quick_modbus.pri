SOURCES += $$PWD/quick_modbus.cpp

HEADERS += $$PWD/quick_modbus.h

INCLUDEPATH *= $$PWD/
DEPENDPATH  *= $$PWD/


#	--- Modbus CRC ---
# например: D:\001-Work\03-Ru\ru_common\daq\3rdparty
SOURCES += $$(MODBUS_CRC)\crc.cpp \
           $$(MODBUS_CRC)\modebus\modbus_crc.c
HEADERS += $$(MODBUS_CRC)\crc.h \
           $$(MODBUS_CRC)\modebus\modbus_crc.h
DEPENDPATH  *= $$(MODBUS_CRC)
INCLUDEPATH *= $$(MODBUS_CRC)
