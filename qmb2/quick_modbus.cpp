#include "quick_modbus.h"

#include "crc.h"

#include <QtCore>
#include <QDebug>

const int RxTimeout = 1000; // ms

Transport::Transport(QObject *parent) : QObject(parent)
{
    _port = new QSerialPort(this);
}

void Transport::setPortName(QString aport)
{
    _port->setPortName(aport);
}

QString Transport::portName()
{
    return _port->portName();
}


// -- параметры COM-порта
bool Transport::setSpeed(int aspeed)
{
    _speed = aspeed;
    _port->setBaudRate(_speed);
    calcTimeouts();
    return true;
}

bool Transport::setDataBits(int adataBits)
{
    if ((adataBits >= 1) || (adataBits < 8)){
        _dataBits = adataBits;
        calcTimeouts();
        _port->setDataBits(QSerialPort::DataBits(_dataBits));
        return true;
    }else {
        return false;
    }
}

bool Transport::setStopBits(float astopBits)
{
    if (astopBits == 1.0)
        _port->setStopBits(QSerialPort::OneStop);
    else if (astopBits == 1.5)
        _port->setStopBits(QSerialPort::OneAndHalfStop);
    else if (astopBits == 2.0)
        _port->setStopBits(QSerialPort::TwoStop);
    else
        return false;

    _stopBits = astopBits;
    calcTimeouts();
    return true;
}

bool Transport::setParity(QSerialPort::Parity aparity)
{
    _parity = aparity;
    _port->setParity(_parity);
    calcTimeouts();
    return true;
}

void Transport::calcTimeouts()
{
    float bits = _dataBits + _stopBits;

    switch (_parity) {
    case QSerialPort::NoParity:
        bits += 0;
        break;
    default:
        bits += 1;
        break;
    }

    const float InterFrameInterval = 3.5; // char
    const int InterFrameDelayMin = 2; // ms

    float charSpeed = _speed/bits;  // char/s
    _interFrameDelayMs = qCeil((InterFrameInterval / charSpeed) * 1000/*ms*/);

    if (_interFrameDelayMs < InterFrameDelayMin)
        _interFrameDelayMs = InterFrameDelayMin;

    //
    _responseTimeoutMs = 1000;
}

bool Transport::open()
{
    return _port->open(QIODevice::ReadWrite);
}

void Transport::close()
{
    _port->close();
}

QString Transport::errorString()
{
    return "Nicht";
}

void Transport::addDevice(MBDevice *adev, QString name)
{
    _devices[name] = adev;
}

MBDevice* Transport::device(QString name)
{
    return _devices.contains(name)? _devices.value(name): nullptr;
}

QHash<QString, MBDevice*> Transport::devices()
{
    return _devices;
}

MBDevice* Transport::takeDevice(QString name)
{
    qDebug() << "Transport::takeDevice(" << name << "); exists?" << _devices.contains(name);

    return _devices.contains(name)? _devices.take(name): nullptr;
}



// ============ MBRequest ============================
MBRequest::MBRequest()
{
    init();
}

MBRequest::MBRequest(MBTable atable, MBRequest::MBRequestMode amode)
{
    init();

    _table = atable;
    _mode = amode;

    _func = _funcs[_mode][_table];
    Q_ASSERT_X(_func > 0, "MBRequest::MBRequest(...)", "Unknown function");
    makePdu();
}

void MBRequest::init()
{
    _table = DiscretesInput;
    _mode = ModeRead;

    _funcs[ModeRead][DiscretesInput] = 0x02;
    _funcs[ModeRead][Coils] = 0x01;
    _funcs[ModeRead][InputRegisters] = 0x04;
    _funcs[ModeRead][HoldingRegister] = 0x03;

    _funcs[ModeWriteMultiple][DiscretesInput] = -1;
    _funcs[ModeWriteMultiple][Coils] = 0x0F;
    _funcs[ModeWriteMultiple][InputRegisters] = -1;
    _funcs[ModeWriteMultiple][HoldingRegister] = 0x10;
}

void MBRequest::setDataTable(MBTable atable)
{
    _table = atable;

    _func = _funcs[_mode][_table];
    Q_ASSERT_X(_func > 0, "MBRequest::setDataTable(...)", "Unknown function");
    makePdu();
}

void MBRequest::setRequestMode(MBRequestMode amode)
{
    _mode = amode;

    _func = _funcs[_mode][_table];
    Q_ASSERT_X(_func > 0, "MBRequest::setRequestMode(...)", "Unknown function");
    makePdu();
}

void MBRequest::setStartAddress(int adr)
{
    _start = adr;

    makePdu();
}

void MBRequest::setDataCount(int count)
{
    _quantity = count;
    makePdu();
}

MBTable MBRequest::dataTable()
{
    return _table;
}

MBRequest::MBRequestMode MBRequest::requestMode()
{
    return _mode;
}

int MBRequest::startAddress()
{
    return _start;
}

int MBRequest::dataCount()
{
    return _quantity;
}

void MBRequest::makePdu()
{
    _pdu.clear();
    _pdu += _func;

    if ((_func >= 0x01) && (_func <= 0x04)){
        // Адрес данных
        _pdu += uchar(_start >> 8); // HiB
        _pdu += uchar(_start); // LoB
        // кол-во данных
        _pdu += uchar(_quantity >> 8); // HiB
        _pdu += uchar(_quantity); // LoB
    }
}

// ============ MBDevice ============================
MBDevice::MBDevice(QString aname)
    : QObject(nullptr)
    , _transport(nullptr)
    , _port(nullptr)
    , _address(-1)
    , _name(aname)
    , _comparator(nullptr)
{
    init();
}

MBDevice::MBDevice(QString aname, Transport *parentTransport)
    : QObject(parentTransport)
    , _transport(parentTransport)
    , _port(nullptr)
    , _address(-1)
    , _name(aname)
    , _comparator(nullptr)
{
    init();

    parentTransport->addDevice(this, _name);
}

void MBDevice::init()
{
    _state = StateIdle;
    _timerTimeout = new QTimer(this);
    _timerTimeout->setSingleShot(true);
    connect(_timerTimeout, SIGNAL(timeout()), this, SLOT(rxTimeoutOccured()));
    _timerInterFrame = new QTimer(this);
    _timerInterFrame->setSingleShot(true);
    connect(_timerInterFrame, SIGNAL(timeout()), this, SLOT(rxInterframeTimeoutOccured()));
}


void MBDevice::setAddress(int adr)
{
    _address = adr;
}

bool MBDevice::sendRequest(MBRequest areq)
{
    qDebug() << "MBDevice::sendRequest(MBRequest areq)";

    if (_state != StateIdle)
        return false;

    _request = areq;

    if (!_transport->port())
        return false;

    // Настраиваем порт
    if (_port != _transport->port()){
        if (_port){
            disconnect(_port, SIGNAL(bytesWritten(qint64)), this, SLOT(processState()));
            disconnect(_port, SIGNAL(readyRead()), this, SLOT(processState()));
            disconnect(_port, SIGNAL(error(QSerialPort::SerialPortError)), this, SLOT(portError(QSerialPort::SerialPortError)));
        }

        _port = _transport->port();
        connect(_port, SIGNAL(bytesWritten(qint64)), this, SLOT(processState()));
        connect(_port, SIGNAL(readyRead()), this, SLOT(processState()));
        connect(_port, SIGNAL(error(QSerialPort::SerialPortError)), this, SLOT(portError(QSerialPort::SerialPortError)));
    }

    _port->clear(QSerialPort::AllDirections);


    if (_comparator)
        delete _comparator;

    _comparator = new MBComparator(areq, _address);

    // переводим автомат в состояние подготовки к отправке
    _state = StateSend;

    _timerInterFrame->setInterval(_transport->interFrameDelayMs());
    _timerTimeout->setInterval(_transport->responseTimeoutMs());

    QTimer::singleShot(0, this, SLOT(processState())); // вызовим метод после возвращения в цикл обработки событий

    return true;
}

MBError MBDevice::error()
{
    return _error;
}

void MBDevice::processState()
{
    qDebug() << "MBDevice::processState(); _state:" << _state;

    switch (_state) {
    case StateIdle:
        qDebug() << "StateIdle";
        break;
    case StateSend:
        qDebug() << "StateSend";
        // Записываем данные в порт
        _port->write(_comparator->requestAdu());
        // устанавливаем новое состояние
        _state = StateSended;
        break;
    case StateSended: // попадём сюда по завершению передачи
        qDebug() << "StateSended";
        if (_address == 0x00) { // широковещательный
            // Запускаем таймер широковещательной паузы / Turnaround delay is from 100 ms to 200 ms.
            QTimer::singleShot(200, this, SLOT(processState()));
            // устанавливаем новое состояние
            _state = StateBroadcastDelay;
        }else {
            // Запускаем таймер таймаута НЕответа
            _timerTimeout->start(RxTimeout);
            // устанавливаем новое состояние
            _state = StateWaitResponse;
        }
        break;
    case StateBroadcastDelay:
        qDebug() << "StateBroadcastDelay";
        // устанавливаем новое состояние
        _state = StateIdle;
        break;
    case StateWaitResponse: {// пытаемся дождаться наше устройство или таймаут неответа
        qDebug() << "StateWaitResponse";
        bool our = false;
        QByteArray buff = _port->readAll();
        while (buff.size() && !our) {
            if (buff.at(0) == _address){
                our = true;
                _state = StateRxInProgress;
                _comparator->appendRxData(buff);
            }else {
                buff.remove(0, 1);
            }
        }
        break;
    }case StateRxInProgress:
        qDebug() << "StateRxInProgress";
        _timerTimeout->stop();
        _timerInterFrame->stop();
        _comparator->appendRxData(_port->readAll());
        if (_comparator->isAduFullLenth()){
            _state = StateRxFinished;
            QTimer::singleShot(0, this, SLOT(processState()));
        }else {
            _timerInterFrame->start();
        }
        break;
    case StateRxFinished:
        qDebug() << "StateRxFinished";
        switch (_comparator->state()) {
        case ErrorNone:
            if (_comparator->isExceptionResponse())
                emit exceptionReceived(_comparator->exceptionCode());
            else
                emit responseReceived(_comparator->response());
            break;
        default:
            emit errorOccurred(_comparator->error());
            break;
        }
        _state = StateIdle;
        QTimer::singleShot(_transport->interFrameDelayMs(), this, SLOT(processState()));
        break;
    case StateResponseTimeout:
        qDebug() << "StateResponseTimeout";
        // Как-то обработать ошибку
        _error._errorString = tr("Устройство не ответило");
        emit errorOccurred(_error);
        _state = StateIdle;
        break;
    case StateInterFrameTimeout:
        qDebug() << "StateInterFrameTimeout";
        // Как-то обработать ошибку
        _error._errorString = tr("Ответ неполный");
        emit errorOccurred(_error);
        _state = StateIdle;
        break;
    default:
        break;
    }
}


void MBDevice::rxTimeoutOccured()
{
    qDebug() << "MBDevice::rxTimeoutOccured()";
    if ((_state != StateRxInProgress) && (_state != StateWaitResponse))
        return;
    _state = StateResponseTimeout;
    processState();
}


void MBDevice::rxInterframeTimeoutOccured()
{
    qDebug() << "MBDevice::rxInterframeTimeoutOccured()";
    if (_state != StateRxInProgress || _state != StateWaitResponse)
        return;
    _state = StateInterFrameTimeout;
    processState();
}

void MBDevice::portError(QSerialPort::SerialPortError aerr)
{
    qDebug() << "Port error:" << aerr;
}

//==================================================================================================

MBComparator::MBComparator(MBRequest &areq, int addr)
{
    // Подготовим данные о свойствах корректного (позитивного или негативного) ответа

    _req = areq;
    _address = addr;

    qDebug() << "MBComparator, PDU:" << _req._pdu;
    Q_ASSERT_X(_req._pdu.size(), "MBComparator", "Reuest PDU empty");

    // -- Негативный ответ
    _pduSizeExept = 1 /*Function code*/ + 1 /*Exception code*/;

    // -- Позитивные ответы
    _func = _req._pdu.at(0);
    switch (_func) {
    case 0x01: { // Read Coils
        int quantityOfCoils = _req._quantity;
        int byteCount = quantityOfCoils/8;
        byteCount +=  (quantityOfCoils%8 ? 1: 0); //
        _pduSizeGood = 1 /*Function code*/ + 1 /*Byte count*/ + byteCount /*Coils Statuse*/;
        break;
    } case 0x02: { // Read Discrete Inputs
        int quantityOfInputs = _req._quantity;
        int byteCount = quantityOfInputs/8;
        byteCount +=  (quantityOfInputs%8 ? 1: 0); //
        _pduSizeGood = 1 /*Function code*/ + 1 /*Byte count*/ + byteCount /*Inputs Statuse*/;
        break;
    } case 0x03: { // Read Holding Registers
        int quantityOfRegisters = _req._quantity;
        int byteCount = quantityOfRegisters * 2;
        _pduSizeGood = 1 /*Function code*/ + 1 /*Byte count*/ + byteCount /*Registers Value*/;
        break;
    } case 0x04: { // Read Input Registers
        int quantityOfRegisters = _req._quantity;
        int byteCount = quantityOfRegisters * 2;
        _pduSizeGood = 1 /*Function code*/ + 1 /*Byte count*/ + byteCount /*Registers Value*/;
        break;
    } case 0x0F: { // Write Multiple Coils
        _pduSizeGood = 1 /*Function code*/ + 2 /*Starting Address*/ + 2 /*Quantity of Outputs*/;
        break;
    } case 0x10: { // Write Multiple Registers
        _pduSizeGood = 1 /*Function code*/ + 2 /*Starting Address*/ + 2 /*Quantity of Outputs*/;
        break;
    } default:
        break;
    }


    // ADU запроса
    _requestAdu.clear();
    _requestAdu += _address;
    _requestAdu += _req._pdu;
    _requestAdu += crc(_requestAdu);

    qDebug() << "MBComparator. ADU:" << _requestAdu.toHex().toUpper();

    _fullLenth = false;
    _state = ErrorNone;
}

QByteArray MBComparator::requestAdu()
{
    return _requestAdu;
}

void MBComparator::appendRxData(const QByteArray &ar)
{
    qDebug() << "MBComparator::appendRxData(" << ar.toHex().toUpper() << ")";

    if (!ar.size())
        return;

    _responseAdu.append(ar);
    // addr
    if (!isOurAddress()){
        _state = ErrorAnotherSlave;
        _error._errorString = QObject::tr("Ответило другое устройство");
        return;
    }

    // -- PDU
    // func
    if (_responseAdu.size() < 2)
        return;
    if (!isOurFunction()){
        _state = ErrorAnotherFunc;
        _error._errorString = QObject::tr("Принят ответ на чужую функцю");
        return;
    }
    // data
    bool ex = _responseAdu.at(1) & 0x80;
    int pduSize = 0;
    if (ex){
        pduSize = _pduSizeExept;
    }else{
        pduSize = _pduSizeGood;
    }
    int aduSize = 1 /*ADDR*/ + pduSize + 2 /*CRC16*/;
    if (_responseAdu.size() < aduSize){
        _state = ErrorIncomplete;
        return;
    }
    // усекаем ADU, игнорируя лишний приём
    _responseAdu.resize(aduSize);
    _fullLenth = true;
    // ADU полная - считаем CRC16
    if (crc(_responseAdu).toInt()){
        _state = ErrorCrcMissmatch;
        _error._errorString = QObject::tr("Ошибка CRC");
        return;
    }

    _error._errorString = QObject::tr("Нет ошибок");
    _state = ErrorNone;
    return;
}

bool MBComparator::isOurAddress()
{
    if (_responseAdu.size() < 1)
        return false;
    return (_responseAdu.at(0) == _address);
}

bool MBComparator::isOurFunction()
{
    if (_responseAdu.size() < 2)
        return false;
    return ((_responseAdu.at(1) & 0x7F) == _func); // без сигнального бита
}

bool MBComparator::isAduFullLenth()
{
    return _fullLenth;
}

bool MBComparator::isExceptionResponse()
{
    return _responseAdu.at(1) & 0x80;
}

int MBComparator::exceptionCode()
{
    return _responseAdu.at(2);
}

QByteArray MBComparator::crc(const QByteArray& array)
{
    return crc16_modbus(array);
}


MBResponse MBComparator::response()
{
    makeResponse();
    return _resp;
}

MBError MBComparator::error()
{
    return _error;
}

void MBComparator::makeResponse()
{
    if (isExceptionResponse())
        return;
    _resp._data.clear();

    QByteArray pdu = _responseAdu.mid(1,_pduSizeGood);

    qDebug() << "makeResponse(), Rx PDU:" << pdu.toHex().toUpper();

    if (_req._mode == MBRequest::ModeWriteMultiple){
        // тут только кол-во записаных элементов возвращается
    }else if (_req._mode == MBRequest::ModeRead){
        // тут возвращаем прочитанные данные
        int byteCount = pdu.at(1);

        switch (_func) {
        case 0x01:
        case 0x02: {
            int byte = 0;
            int adr = 0;
            while (byte < byteCount) {
                for (int bit = 0; bit < 8; ++bit) {
                    uchar mask = 1 << bit;
                    _resp._data[adr++] = bool(pdu.at(2 + byte) & mask);
                }
                byte++;
            }
            break;
        }case 0x03:
         case 0x04: {
            int adr = 0;
            int registerCount = byteCount / 2;
            int index = 0;
            while (index < registerCount) {
                QByteArray data = pdu.mid(2 + index*2, 2);
                quint16 value = 0;
                value |= quint16(uchar(data.at(0))) << 8;
                value |= quint16(uchar(data.at(1)));
                _resp._data[adr++] = int(value);
                index++;
            }
            break;
        }default:
            break;
        }
    }else {
        // тут неизвестная ситуация
    }
}

